class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  private
  # Método para impedir que el usuario ingrese a /post sin estar autenticado.
  def authenticate_user!
    redirect_to users_sign_in_path unless helpers.logged?
  end
end
