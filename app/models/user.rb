class User < ApplicationRecord
  has_secure_password
=begin
  # Validando que el usuario sea único
  validates :email, uniqueness: true

  has_many :posts

  # Atributos virtuales
  attr_accessor :password, :password_confirmation
  validates :password, presence: true
  validates :password, confirmation: true

  # Encriptando password
  before_save :bcrypt_password
  def bcrypt_password
    self.password_digest = BCrypt::Password.create password
  end

  # Comparando password
  def password?(p)
    BCrypt::Password.new(password_digest) == p
  end
=end
end
